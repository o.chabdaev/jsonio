import json
from pathlib import Path

data_path = Path('..', 'data')
new_folder_path = Path(data_path, 'new_folder')
Path.mkdir(new_folder_path, parents=True, exist_ok=True)
for json_path in data_path.glob('*.json'):
    with open(json_path, 'r') as file:
        data = json.load(file)
    with open(Path(new_folder_path, f'output_{json_path.name}'), 'w') as file:
        json.dump(data, file, indent=4)
